#!/bin/python

import numpy as np
from pandas import *
from sklearn.tree import DecisionTreeClassifier

titanicData = read_csv("titanic.csv")
dataWithNans = titanicData.loc[:,['Pclass', 'Fare', 'Sex', 'Age', 'Survived']]

dataWithNans['Sex'] = dataWithNans['Sex'].map({'female': 0, 'male': 1})

data = dataWithNans[np.isnan(dataWithNans['Age']) == False]
features = data[['Pclass', 'Fare', 'Sex', 'Age']].as_matrix()
target = data['Survived'].values

#print features

clf = DecisionTreeClassifier(random_state = 241)
clf.fit(features, target)

importances = clf.feature_importances_
print importances
