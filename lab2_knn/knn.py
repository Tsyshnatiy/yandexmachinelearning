import numpy as np
from sklearn import cross_validation
from sklearn import neighbors
from sklearn import preprocessing

def crossValidation(features, target):
	scores = []

	for k in range(1, 50):
		kf = cross_validation.KFold(target.size, n_folds = 5,
									shuffle = True,
									random_state = 42)
									
		neigh = neighbors.KNeighborsClassifier(k, weights = 'uniform');
		scoreMean = cross_validation.cross_val_score(neigh, features, target, 
												  scoring = 'accuracy',
												  cv = kf).mean()
		scores.append(scoreMean)
		

	print max(scores)
	print range(1,50)[scores.index(max(scores))]

df = np.genfromtxt('wine.data', delimiter=',') 
features = df[:,1:]
target = df[:, 0]

crossValidation(features, target)

features = preprocessing.scale(features)
crossValidation(features, target)
