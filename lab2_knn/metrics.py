import numpy as np
from sklearn import datasets, preprocessing
from sklearn import cross_validation, neighbors

dataset = datasets.load_boston()

features = preprocessing.scale(dataset.data)
target = dataset.target

ps = np.linspace(1, 10, num=200)

scores = []

for pp in ps:	
	kf = cross_validation.KFold(target.size, n_folds = 5, random_state = 42)						
	neigh = neighbors.KNeighborsRegressor(n_neighbors = 5,
										  weights = 'distance',
										  p = pp);
										  
	scoresMean = cross_validation.cross_val_score(neigh, features, target,
											scoring = 'mean_squared_error',
											cv = kf).mean()
	scores.append(scoresMean)

print max(scores)
print ps[scores.index(max(scores))]
