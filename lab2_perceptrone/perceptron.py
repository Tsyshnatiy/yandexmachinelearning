import numpy as np
import pandas as pd
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler

trainData = pd.read_csv("perceptron_train.csv", header = None)
testData = pd.read_csv("perceptron_test.csv", header = None)

targetTrain = trainData.loc[:,[0]].values.flatten()
featuresTrain = trainData.loc[:,[1,2]].as_matrix()

targetTest = testData.loc[:,[0]].values.flatten()
featuresTest = testData.loc[:,[1,2]].as_matrix()

perc = Perceptron(random_state=241)
perc.fit(featuresTrain, targetTrain)
predictionsTrain = perc.predict(featuresTrain)

unscaledAccuracyTrain = accuracy_score(targetTrain, predictionsTrain)
print unscaledAccuracyTrain

scaler = StandardScaler()
featuresTrainScaled = scaler.fit_transform(featuresTrain)
featuresTestScaled = scaler.fit_transform(featuresTest)

perc.fit(featuresTrainScaled, targetTrain)
predictionsTest = perc.predict(featuresTestScaled)

unscaledAccuracyTest = accuracy_score(targetTest, predictionsTest)
print unscaledAccuracyTest

print unscaledAccuracyTest - unscaledAccuracyTrain
