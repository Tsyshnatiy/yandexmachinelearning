from pandas import *
import math
from sklearn.metrics import roc_auc_score

def grad_down(target, data, step = 0.1, C = 0):
	w1Last = 100000;
	w2Last = 100000;
	w1 = 0
	w2 = 0
	l = target.size
	for j in range(1, 10000):
		dist = math.sqrt((w1Last - w1) ** 2 + (w2Last - w2) ** 2)
		if (dist < 1e-5):
			return w1, w2
			
		sumW1 = 0
		sumW2 = 0
		for i in range(1, l):
			x1i = data[i, 0]
			x2i = data[i, 1]
			yi = target[i]
			exp = 1.0 / (1 + math.exp(-1 * yi * (w1 * x1i + w2 * x2i)))
			sumW1 = sumW1 + yi * x1i * (1 - exp)
			sumW2 = sumW2 + yi * x2i * (1 - exp)
		w1Last = w1;
		w2Last = w2;
		w1 = w1 + step / l * sumW1 - C * step * w1
		w2 = w2 + step / l * sumW2 - C * step * w2

data = read_csv("data_log.csv", header=None)
target = data.loc[:,[0]].values.flatten()
features = data.loc[:,[1,2]].as_matrix()

wNoReg = grad_down(target, features)
wReg = grad_down(target, features, C = 10)

aNoRegExpPower = np.array(-1 * (wNoReg[0] * features[:, 0] + wNoReg[1] * features[:, 1]))
aNoReg = 1.0 / (1 + np.exp(aNoRegExpPower))

aRegExpPower = np.array(-1 * (wReg[0] * features[:, 0] + wReg[1] * features[:, 1]))
aReg = 1.0 / (1 + np.exp(aRegExpPower))

print roc_auc_score(target, aNoReg)
print roc_auc_score(target, aReg)
