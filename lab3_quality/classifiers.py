import numpy as np
from pandas import *
from sklearn.metrics import roc_auc_score
from sklearn.metrics import precision_recall_curve

data = read_csv("scores.csv")
true = data.loc[:,['true']].values.flatten()

score_logreg = data.loc[:,['score_logreg']].values.flatten()
score_svm = data.loc[:,['score_svm']].values.flatten()
score_knn = data.loc[:,['score_knn']].values.flatten()
score_tree = data.loc[:,['score_tree']].values.flatten()

names = ["score_logreg", "score_svm", "score_knn", "score_tree"]
ys = [score_logreg, score_svm, score_knn, score_tree];

def checkClassifier(t, y):
	precision, recall, th = precision_recall_curve(t, y)
	
	maxPrec = -1;
	for i in range (0, precision.size):
		if recall[i] > 0.7 and precision[i] > maxPrec:
			maxPrec = precision[i]
	return maxPrec

def getBestClassifierName(t, ys, yNames):
	bestPrec = -1
	bestName = yNames[0]
	for i in range(0, len(ys)):
		bestCurPrec = checkClassifier(t, ys[i])
		if bestCurPrec > bestPrec:
			bestPrec = bestCurPrec
			bestName = yNames[i]
	return bestName

def getBestRoc(names, t, ys):
	bestRoc = 0
	bestRocName = names[0]
	for i in range(0, len(ys)):
		curRocScore = roc_auc_score(t, ys[i])
		if bestRoc < curRocScore:
			bestRoc = curRocScore
			bestRocName = names[i]
	return bestRocName
	
	
print getBestRoc(names, true, ys)
print getBestClassifierName(true, ys, names)

