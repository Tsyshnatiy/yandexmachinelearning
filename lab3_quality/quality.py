import numpy as np
from pandas import *
from sklearn.metrics import recall_score, precision_score, f1_score, accuracy_score

data = read_csv("classification.csv")
true = data.loc[:,['true']].values.flatten()
predicted = data.loc[:,['pred']].values.flatten()

def calcTpfn(e1, e2, t, p):
	res = 0
	for i in range(0, t.size):
		if t[i] == e1 and p[i] == e2:
			res = res + 1
	return res
	
Tp = calcTpfn(1, 1, true, predicted)
Fp = calcTpfn(0, 1, true, predicted)
Fn = calcTpfn(1, 0, true, predicted)
Tn = calcTpfn(0, 0, true, predicted)

print Tp, Fp, Fn, Tn

#accuracy = float(Tp + Tn) / (Tp + Fp + Fn + Tn)
#recision = float(Tp) / (Tp + Fp)
#recall = float(Tp) / (Tp + Fn)
#F = 2 * precision * recall / (recall + precision)

print accuracy_score(true, predicted), precision_score(true, predicted)
print recall_score(true, predicted), f1_score(true, predicted)
