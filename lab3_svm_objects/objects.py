import numpy as np
from pandas import *
from sklearn.svm import SVC

trainData = read_csv("svm-data.csv", header=None)
targetTrain = trainData.loc[:,[0]].values.flatten()
featuresTrain = trainData.loc[:,[1,2]].as_matrix()

clf = SVC(C=100000, random_state=241, kernel='linear')
clf.fit(featuresTrain, targetTrain) 

print clf.support_
