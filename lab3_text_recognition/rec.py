import numpy as np
from sklearn import datasets
from sklearn.svm import SVC
from sklearn.cross_validation import KFold
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.grid_search import GridSearchCV

import unicodedata

newsgroups = datasets.fetch_20newsgroups(
                    subset='all', 
                    categories=['alt.atheism', 'sci.space'])

vectorizer = TfidfVectorizer()
y = vectorizer.fit_transform(newsgroups.data)

#grid = {'C': np.power(10.0, np.arange(-5, 6))}
#cv = KFold(newsgroups.target.size, n_folds=5, shuffle=True, random_state=241)
clf = SVC(kernel='linear', random_state=241, C=10)
#gs = GridSearchCV(clf, grid, scoring='roc_auc', cv=cv)
#gs.fit(y, newsgroups.target)

#for a in gs.grid_scores_:
#    print a.mean_validation_score
#    print a.parameters
#    print ""

clf.fit(y, newsgroups.target)

top10Ind = np.argsort(np.absolute(np.asarray(clf.coef_.todense())).reshape(-1))[-10:]

top10Words = []
featureNames = vectorizer.get_feature_names()
for i in range(0, top10Ind.size):
	word = unicodedata.normalize('NFKD', featureNames[top10Ind[i]]).encode('ascii','ignore')
	top10Words.append(word)
	
top10Sorted = sorted(top10Words)

result = ""
for i in range(0, len(top10Sorted)):
	result = result + top10Sorted[i]
	if i < len(top10Sorted) - 1:
		result = result + ','
	
print result 
