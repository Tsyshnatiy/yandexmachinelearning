from pandas import *
import numpy as np
from sklearn.decomposition import PCA

trainData = read_csv("close_prices.csv")
trainData.drop('date', axis=1, inplace=True)

pca = PCA(n_components=10)
pca.fit(trainData)

# 4 components is enough to explain 90% variation
print pca.explained_variance_ratio_

tDataFirstComp = pca.transform(trainData)[:,0]

dowJonesData = read_csv("djia_index.csv")
dowJonesData.drop('date', axis=1, inplace=True)
dowJonesData = dowJonesData.values.flatten()

print np.corrcoef(tDataFirstComp, dowJonesData)

# 0 is index of max component in explained_variance_ratio_
maxComp = max(pca.components_[0])

# index of company 
print pca.components_.shape

coolestCompanyIdx = np.where(pca.components_[0] == maxComp)[0]
print trainData.columns.values[coolestCompanyIdx]
