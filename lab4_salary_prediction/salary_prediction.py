from pandas import *
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction import DictVectorizer
import scipy as scp
from sklearn.linear_model import Ridge
import numpy as np


trainData = read_csv("salary_train.csv")
trainData['FullDescription'] = trainData['FullDescription'].str.lower()
trainData['LocationNormalized'] = trainData['LocationNormalized'].str.lower()
trainData['FullDescription'] = trainData['FullDescription'].replace('[^a-zA-Z0-9]', ' ', regex = True)

enc = DictVectorizer()
vectorizer = TfidfVectorizer(min_df=5, max_df=1000000)
trainDataFd_tf = vectorizer.fit_transform(trainData['FullDescription'])

trainData['LocationNormalized'].fillna('nan', inplace=True)
trainData['ContractTime'].fillna('nan', inplace=True)

trainCateg = enc.fit_transform(trainData[['LocationNormalized', 'ContractTime']].to_dict('records'))

joinedTrainData = scp.sparse.hstack([trainDataFd_tf, trainCateg])

clf = Ridge(alpha = 1.0)
clf.fit(joinedTrainData, trainData['SalaryNormalized'])

testData = read_csv("salary_test.csv")
testData['FullDescription'] = testData['FullDescription'].str.lower()
testData['LocationNormalized'] = testData['LocationNormalized'].str.lower()
testData['FullDescription'] = testData['FullDescription'].replace('[^a-zA-Z0-9]', ' ', regex = True)
testDataFd_tf = vectorizer.transform(testData['FullDescription'])
testData['LocationNormalized'].fillna('nan', inplace=True)
testData['ContractTime'].fillna('nan', inplace=True)
testCateg = enc.transform(testData[['LocationNormalized', 'ContractTime']].to_dict('records'))
joinedTestData = scp.sparse.hstack([testDataFd_tf, testCateg])

print clf.predict(joinedTestData)
