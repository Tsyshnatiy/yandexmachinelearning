from pandas import *
import numpy as np
from sklearn import cross_validation, ensemble, metrics
import matplotlib.pyplot as plt

trainOrig = read_csv("gbm_data.csv")

target = trainOrig.Activity.values

trainOrig.drop('Activity', axis=1, inplace=True)

features = trainOrig.values
xTrain, xTest, yTrain, yTest = cross_validation.train_test_split(features, target, test_size = 0.8, 
												random_state = 241)
'''
for lr in [1, 0.5, 0.3, 0.2, 0.1]:
	gbc = ensemble.GradientBoostingClassifier(n_estimators = 250, verbose = True,
										learning_rate = lr, random_state = 241)
										
	gbc.fit(xTrain, yTrain)
	
	lossTrain = np.array([])
	lossTest = np.array([])
	for i, pred in enumerate(gbc.staged_decision_function(xTrain)):
		probas = float(1) / (1 + np.exp(-1 * pred))
		lossTrain = np.append(lossTrain, metrics.log_loss(yTrain, probas))
		
	for i, pred in enumerate(gbc.staged_decision_function(xTest)):
		probas = float(1) / (1 + np.exp(-1 * pred))
		lossTest = np.append(lossTest, metrics.log_loss(yTest, probas))
	
	if lr == 0.2:
		print np.min(lossTest)
		print np.where(lossTest == np.min(lossTest))
	
	xAxis = np.linspace(0, lossTest.size - 1, lossTest.size)
	plt.figure()
	plt.plot(xAxis, lossTest, 'r', linewidth=2)
	plt.plot(xAxis, lossTrain, 'g', linewidth=2)
	plt.legend(['test', 'train'])
	
#plt.show()
'''

bestBoostingQualityIter = 37
regressor = ensemble.RandomForestClassifier(bestBoostingQualityIter, random_state = 241)
regressor.fit(xTrain, yTrain)
pred = regressor.predict_proba(xTest)
loss = metrics.log_loss(yTest, pred)
print loss
