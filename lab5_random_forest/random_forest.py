from pandas import *
import numpy as np
from sklearn import cross_validation, ensemble, metrics

train = read_csv("abalone.csv")
train['Sex'] = train['Sex'].map(lambda x: 1 if x == 'M' else (-1 if x == 'F' else 0))

target = train['Rings'].values.flatten()
train.drop('Rings', axis=1, inplace=True)
features = train.as_matrix()

print target
print features

for i in range(1, 50):
	kf = cross_validation.KFold(target.size, n_folds = 5, random_state = 1, shuffle=True)	
	regressor = ensemble.RandomForestRegressor(i, random_state=1)
	score = cross_validation.cross_val_score(regressor, features, target,
												scoring = 'r2', cv = kf).mean()
	print i, score
	if score > 0.52:
		break;
