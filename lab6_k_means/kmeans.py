import skimage
import math
from skimage import io
from sklearn import cluster
import numpy as np

image = io.imread('parrots.jpg')
image = skimage.img_as_float(image)
imageBackup = np.copy(image)

def getObjFeaturesMatrix(img):
	r = np.array([img[:,:,0].ravel()]).T;
	g = np.array([img[:,:,1].ravel()]).T;
	b = np.array([img[:,:,2].ravel()]).T;
	result = np.hstack((r, g))
	return np.hstack((result, b))

mtx = getObjFeaturesMatrix(image)
clustersNum = 20
kmeans = cluster.KMeans(n_clusters = clustersNum, init = 'k-means++', random_state = 241)
kmeans.fit(mtx)

labels = kmeans.labels_.reshape((image.shape[0], image.shape[1]))

def processMean(img, lbls, means, clustersNumber):
	for cluster in range(0, clustersNumber):
		img[lbls == cluster] = means[cluster]
		
	return img

imageMean = processMean(image, labels, kmeans.cluster_centers_ , clustersNum)

def calcPsnr(pattern, processed):
	mse = np.mean((pattern - processed) ** 2)
	return 10 * math.log10(float(1) / mse)

print calcPsnr(imageBackup, imageMean)
io.imshow(imageMean)
io.show()

